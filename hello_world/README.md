# Hello world!

## Compilation
Compile it with ```ghc -o hello hello.hs```.

This repo represents the result of a simple compilation.

To learn more about the compilation process, [Read this](https://downloads.haskell.org/~ghc/7.0.3/docs/html/users_guide/separate-compilation.html)

## Files
- ```hello.hi``` Interface file. Use ```ghc --show-iface hello.hi``` to read it's contents.
- ```hello.o``` Object file.
